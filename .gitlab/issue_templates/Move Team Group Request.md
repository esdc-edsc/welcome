<!-- 
This template is so people can request their existing team group is moved under this parent group
Please provide the following details
-->

### Team Group details

**Current Group location:** <!-- link to group -->

**Team Name:**  
**Team Acronym:**  
**Team Leader:** <!-- link GitLab account -->

**Other Team Members:**
<!-- link GitLab account, or departmental email -->

<!--
    NOTE: You will need to give "Owner" rights to the user actioning this request on your existing group.
-->

/label ~move ~team