<!-- 
This template is so people can request a new team group is created under this parent group
Please provide the following details
-->

### Team Group details

**Team Name:**  
**Team Acronym:**  
**Team Leader:** <!-- link GitLab account -->

**Other Team Members:**
<!-- link GitLab account, or departmental email -->

/label ~new ~team