# How to contribute

Helping keep this project up to date, helps others know what's going on in the IITB world and where to find needed information. So thank you for helping to maintain it!

## Getting involved

If you're new to GitLab or Git don't worry, open an issue asking for some help and what you want to do. We'll be happy to help.

### Reporting

Reporting new additions though issues is key. This helps us keep track of all the work that is needed and wanted for this project.

When opening new issues please use the template provided if they fit your request.  
Templates avaliable:

* **New Team Group Request** - requesting to create a new group under this parent group.
* **Move Team Group Request** - requesting to move a team group under this parent group.
* **List Team Group** - We have a separate team group that should be added to the list.

### Developing

#### Contributors

You can fork the project, and make a merge request with your changes.  

Please use the **Gernal** merge request template.