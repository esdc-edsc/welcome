# Welcome

Defining policys and recommendations for repos under this Organization.

Any teams in Employment and Scotial Development Canada that want to host code under this organization are welcome to. Please open an issue in this project with the template provided to get your group added.

## Other Ogranization Groups

ESDC has a few other organization groups on diffrent platforms where teams can also store code.

* [GCCode](https://gccode.ssc-spc.gc.ca/iitb-dgiit) (internal)
* [GitHub](https://github.com/esdc-edsc)

## Starter Guides

We have a number of starter projects that will help you start developing in GCCode or other Git Platforms.

* [Security Requirements](guides/security.md) - These will help you keep your code safe, and are just good practices to have.
* [ESDC Labels](https://gccode.ssc-spc.gc.ca/iitb-dgiit/esdc-labels) (internal) - Quickly add standard labels to your project that you will use every day to track your work
* [ESDC Templates](https://gccode.ssc-spc.gc.ca/iitb-dgiit/esdc-templates) (internal) - Guides for writing standard files all Git repos expect to have.
* [ESDC Development Setup](https://gitlab.com/esdc-edsc/esdc-development-setup) - Making sure you can connect to External Git repos. It also has some Git tricks.

## Team Groups 

### Under this parent group

* N/A

### Outside this group

* N/A